package dhbw.web.qaa;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import dhbw.web.interfaces.service.AnswerService;
import dhbw.web.interfaces.service.NotificationService;
import dhbw.web.model.Answer;
import dhbw.web.model.User;
import dhbw.web.model.db.AnswerDB;
import dhbw.web.model.db.repository.AnswerDBRepository;
import dhbw.web.model.form.CreateAnswerForm;
import dhbw.web.model.form.NotificationType;
import dhbw.web.model.form.PushNotificationForm;
import dhbw.web.model.to.AnswerTO;
import dhbw.web.utils.MapperWrapper;
import lombok.extern.slf4j.Slf4j;

@RestController
@RequestMapping(path = "/answer")
@Slf4j
public class AnswerController implements AnswerService {

	@Autowired
	private NotificationService notifyService;

	@Autowired
	private QuestionController questionService;

	@Autowired
	private AnswerDBRepository answerDBRepository;

	@Autowired
	private MapperWrapper dbMapper;

	@LoadBalanced
	@PostMapping("")
	@Override
	public Answer createAnswer(@RequestBody CreateAnswerForm form) {
		log.debug("create answer: " + form);
		User user = form.getUser();
		Answer answer = new Answer();
		answer.setCreated(form.getAnswer().getCreated());
		answer.setOwner(user);
		answer.setParentQuestion(form.getParent());
		answer.setId(1);
		answer.setText(form.getAnswer().getText());
		this.notifyService.publish(new PushNotificationForm(answer, answer.getOwner(), NotificationType.CREATE));
		answer.setId(answerDBRepository.save(dbMapper.map(answer, AnswerDB.class)));
		return answer;
	}

	@LoadBalanced
	@GetMapping("/q")
	@Override
	public Answer[] getQuestionAnswers(@RequestParam("id") int id) {
		log.debug("get all answers of question with id: " + id);
		List<Answer> answers = dbMapper.map(answerDBRepository.findQuestionAnswers(id), AnswerDB.class, Answer.class);
		return answers.toArray(new Answer[answers.size()]);
	}

	@LoadBalanced
	@GetMapping("/{id}")
	@Override
	public Answer readAnswer(@PathVariable("id") int id) {
		log.debug("get an answer by id: " + id);
		AnswerDB a = answerDBRepository.findById(id);
		if (a == null) {
			return new Answer();
		}
		return dbMapper.map(a, Answer.class);
	}

	@LoadBalanced
	@PutMapping("")
	@Override
	public void updateAnswer(@RequestBody AnswerTO answer) {
		log.debug("update the following answer: " + answer);

		AnswerDB db = answerDBRepository.findById(answer.getId());
		if (db == null) {
			return;
		}
		// TODO read answer from db
		Answer a = dbMapper.map(db, Answer.class);
		// TODO update answer
		a.setText(answer.getText());

		// TODO write back to db
		this.notifyService.publish(new PushNotificationForm(a, answer.getUserId(), NotificationType.UPDATE));
		answerDBRepository.save(dbMapper.map(a, AnswerDB.class));
	}

	@LoadBalanced
	@DeleteMapping("/{id}")
	@Override
	public void deleteAnswer(@PathVariable("id") int id) {
		log.debug("delete the answer with id: " + id);
		answerDBRepository.deleteById(id);
	}

	@LoadBalanced
	@GetMapping("")
	@Override
	public Answer[] readAllAnswers() {
		log.debug("read all answers");
		List<Answer> alla = new ArrayList<>();
		Iterable<AnswerDB> adb = answerDBRepository.findAll();
		for (AnswerDB answerDB : adb) {
			alla.add(dbMapper.map(answerDB, Answer.class));
		}
		return alla.toArray(new Answer[alla.size()]);
	}

}
