package dhbw.web.qaa;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.context.annotation.Bean;
import org.springframework.web.client.RestTemplate;

import dhbw.web.interfaces.service.NotificationService;
import dhbw.web.model.db.mapper.ModelDBMapper;
import dhbw.web.model.db.repository.AnswerDBRepository;
import dhbw.web.model.db.repository.CommentDBRepository;
import dhbw.web.model.db.repository.QuestionDBRepository;
import dhbw.web.service.NotificationServiceImpl;
import dhbw.web.utils.MapperWrapper;

@EnableDiscoveryClient
@SpringBootApplication(scanBasePackages = { "dhbw.web.qaa", "dhbw.web.service", "dhbw.web.model.db.mapper" })
public class QaaApplication {

	public static void main(String args[]) {
		SpringApplication.run(QaaApplication.class, args);
	}

	@Bean
	public NotificationService notifyService() {
		return new NotificationServiceImpl();
	}

	@Bean
	@LoadBalanced
	public RestTemplate restTemplate() {
		return new RestTemplate();
	}

	@Bean
	public MapperWrapper mapper() {
		return ModelDBMapper.getModelDBMapper();
	}

	@Bean
	public QuestionDBRepository questionRepo() {
		return new QuestionDBRepository();
	}

	@Bean
	public AnswerDBRepository answerRepo() {
		return new AnswerDBRepository();
	}

	@Bean
	public CommentDBRepository commentRepo() {
		return new CommentDBRepository();
	}

}
