package dhbw.web.qaa;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import dhbw.web.interfaces.service.NotificationService;
import dhbw.web.interfaces.service.QuestionService;
import dhbw.web.model.Question;
import dhbw.web.model.db.QuestionDB;
import dhbw.web.model.db.repository.QuestionDBRepository;
import dhbw.web.model.form.CreateQuestionForm;
import dhbw.web.model.form.NotificationType;
import dhbw.web.model.form.PushNotificationForm;
import dhbw.web.model.to.QuestionTO;
import dhbw.web.utils.MapperWrapper;
import lombok.extern.slf4j.Slf4j;

@RestController
@RequestMapping(path = "/question")
@Slf4j
public class QuestionController implements QuestionService {

	@Autowired
	private NotificationService notifyService;

	@Autowired
	private QuestionDBRepository questionrepository;

	@Autowired
	private MapperWrapper dbMapper;

	@LoadBalanced
	@PostMapping("")
	@Override
	public Question createQuestion(@RequestBody CreateQuestionForm form) {
		log.debug("create question from form: " + form);
		Question question = new Question(form.getQuestion().getText(), form.getQuestion().getText(), form.getUser());
		this.notifyService.publish(new PushNotificationForm(question, question.getOwner(), NotificationType.CREATE));
		question.setId(this.questionrepository.save(dbMapper.map(question, QuestionDB.class)));
		return question;
	}

	@LoadBalanced
	@GetMapping("")
	@Override
	public Question[] getAllQuestions() {
		log.debug("read all questions");
		Iterable<QuestionDB> alldb = questionrepository.findAll();
		List<Question> all = new ArrayList<>();
		for (QuestionDB question : alldb) {
			all.add(dbMapper.map(question, Question.class));
		}
		Question[] arr = new Question[all.size()];
		if (all.size() != 0) {
			arr = all.toArray(arr);
		}
		return arr;
	}

	@LoadBalanced
	@GetMapping("/id/{id}")
	@Override
	public Question readQuestion(@PathVariable("id") int id) {
		log.debug("read question by id: " + id);
		QuestionDB q = questionrepository.findById(id);
		if (q == null) {
			return new Question();
		}
		return dbMapper.map(q, Question.class);
	}

	@LoadBalanced
	@GetMapping("/url/{url}")
	@Override
	public Question readQuestionByUrl(@PathVariable("url") String url) {
		log.debug("read question by url: " + url);

		QuestionDB q = questionrepository.findByUrl(url);
		if (q == null) {
			return new Question();
		}
		return dbMapper.map(q, Question.class);
	}

	@LoadBalanced
	@PutMapping("")
	@Override
	public void updateQuestion(@RequestBody QuestionTO question) {
		log.debug("update question with: " + question);

		// TODO read from db via ID
		Question q = new Question();
		QuestionDB db = questionrepository.findById(question.getId());
		if (db == null) {
			return;
		}
		q = dbMapper.map(db, Question.class);
		// TODO update original if set
		q.setUrl(question.getUrl().equals("") ? q.getUrl() : question.getUrl());
		q.setTitle(question.getTitle().equals("") ? q.getTitle() : question.getTitle());
		q.setText(question.getText().equals("") ? q.getText() : question.getText());
		// TODO write back to db
		questionrepository.save(dbMapper.map(q, QuestionDB.class));
		this.notifyService.publish(new PushNotificationForm(q, question.getUserId(), NotificationType.UPDATE));
	}

	@LoadBalanced
	@DeleteMapping("/{id}")
	@Override
	public void deleteQuestion(@PathVariable("id") int id) {
		log.debug("delete question by id: " + id);
		questionrepository.deleteById(id);
	}

}
