package dhbw.web.qaa;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import dhbw.web.interfaces.service.CommentService;
import dhbw.web.interfaces.service.NotificationService;
import dhbw.web.model.Comment;
import dhbw.web.model.db.CommentDB;
import dhbw.web.model.db.repository.CommentDBRepository;
import dhbw.web.model.form.CreateCommentForm;
import dhbw.web.model.form.NotificationType;
import dhbw.web.model.form.PushNotificationForm;
import dhbw.web.model.to.CommentTO;
import dhbw.web.utils.MapperWrapper;
import lombok.extern.slf4j.Slf4j;

@RestController
@RequestMapping(path = "/comment")
@Slf4j
public class CommentController implements CommentService {

	@Autowired
	private NotificationService notifyService;

	@Autowired
	private CommentDBRepository commentDbRepository;

	@Autowired
	private MapperWrapper dbMapper;

	@LoadBalanced
	@GetMapping("/e")
	@Override
	public Comment[] getElemnetComments(@RequestParam("id") int id, @RequestParam("type") String type) {
		log.debug(String.format("get comments from parent id %d of type %s", id, type));
		List<Comment> comment = dbMapper.map(commentDbRepository.findByElement(id, type), CommentDB.class,
				Comment.class);
		return comment.toArray(new Comment[comment.size()]);
	}

	@LoadBalanced
	@GetMapping("")
	@Override
	public Comment[] getAllComments() {
		log.debug("read all comments");
		List<Comment> cl = new ArrayList<>();
		Iterable<CommentDB> cdb = commentDbRepository.findAll();
		for (CommentDB commentDB : cdb) {
			cl.add(dbMapper.map(commentDB, Comment.class));
		}
		return cl.toArray(new Comment[cl.size()]);
	}

	@LoadBalanced
	@PostMapping("")
	@Override
	public Comment createComment(@RequestBody CreateCommentForm form) {
		log.debug("create comment");
		Comment comment = new Comment(form.getComment().getText(), form.getUser(), form.getParent());
		this.notifyService.publish(new PushNotificationForm(comment, form.getUser(), NotificationType.CREATE));
		comment.setId(commentDbRepository.save(dbMapper.map(comment, CommentDB.class)));
		return comment;

	}

	@LoadBalanced
	@GetMapping("/{id}")
	@Override
	public Comment readComment(@PathVariable("id") int id) {
		log.debug("read comment with id: " + id);
		CommentDB c = commentDbRepository.findById(id);
		if (c == null) {
			return new Comment();
		}
		return dbMapper.map(c, Comment.class);
	}

	@LoadBalanced
	@PutMapping("")
	@Override
	public void updateComment(CommentTO comment) {
		log.debug("update the comment with the following information " + comment);

		// TODO read from db
		CommentDB db = commentDbRepository.findById(comment.getId());
		if (db == null) {
			return;
		}
		Comment c = dbMapper.map(db, Comment.class);
		// TODO udpate
		c.setText(comment.getText());
		// TODO write back to db
		commentDbRepository.save(dbMapper.map(c, CommentDB.class));
		this.notifyService.publish(new PushNotificationForm(c, comment.getUserId(), NotificationType.UPDATE));
	}

	@LoadBalanced
	@DeleteMapping("/{id}")
	@Override
	public void deleteComment(@PathVariable("id") int id) {
		log.debug("delete comment with id: " + id);
		commentDbRepository.deleteById(id);
	}

}
